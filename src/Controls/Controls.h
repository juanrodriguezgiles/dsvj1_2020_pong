#ifndef CONTROLS_H
#define CONTROLS_H
#include "raylib.h"
namespace Controls {
	extern int player1UP;
	extern int player1DOWN;
	extern int player2UP;
	extern int player2DOWN;

	extern int aux;
	extern bool waiting;
}
#endif