#include "Rackets.h"
using namespace Ball;
namespace Rackets {
	 Rectangle racket1;
	 Rectangle racket2;
	 Color racketColor = WHITE;
	 int racketCurrentColor = 0;
	 int racket1Score = 0;
	 int racket2Score = 0;
	 int racket1RoundsWon = 0;
	 int racket2RoundsWon = 0;
	 const float racketYSpeed = 3.0f;
	 bool modified = false;

	 void checkForCollisionWithRackets() {
		 if (counter == -1) {
			 if (CheckCollisionCircleRec(ballPosition, ballRadius, racket1)) {
				 ballSpeed.x *= -1.0f;
				 ballSpeed.y *= -1.0f;
				 counter++;
			 }
			 if (CheckCollisionCircleRec(ballPosition, ballRadius, racket2)) {
				 ballSpeed.x *= -1.0f;
				 ballSpeed.y *= -1.0f;
				 counter++;
			 }
		 }
		 if (counter > -1 && counter <= 6) {
			 counter++;
		 }
		 else if (counter > 6) {
			 counter = -1;
		 }
	 }

	 void resetRackets() {
		 racket1.x = 50;
		 racket1.y = (screenHeight / 2 + reservedSpace) - racket1.height / 2;

		 racket2.x = screenWidth - racket2.width - 50;
		 racket2.y = (screenHeight / 2 + reservedSpace) - racket2.height / 2;
	 }

	 bool checkForWin() {
		 if (racket1Score >= 5) {
			 racket1RoundsWon++;
		 }
		 else if (racket2Score >= 5) {
			 racket2RoundsWon++;
		 }
		 return (racket1Score >= 5 || racket2Score >= 5);
	 }

	 void moveRacket1Up() {

		 racket1.y -= racketYSpeed;
	 }

	 void moveRacket1Down() {

		 racket1.y += racketYSpeed;
	 }

	 void moveRacket2Up() {

		 racket2.y -= racketYSpeed;
	 }

	 void moveRacket2Down() {

		 racket2.y += racketYSpeed;
	 }
}