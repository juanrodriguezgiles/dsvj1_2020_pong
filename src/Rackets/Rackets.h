#ifndef RACKETS_H
#define RACKETS_H
#include "raylib.h"
#include "ball/ball.h"
#include "configuration/screen.h"
namespace Rackets {
	extern Rectangle racket1;
	extern Rectangle racket2;
	extern Color racketColor;
	extern int racketCurrentColor;
	extern int racket1Score;
	extern int racket2Score;
	extern int racket1RoundsWon;
	extern int racket2RoundsWon;
	extern const float racketYSpeed;
	extern bool modified;

	void checkForCollisionWithRackets();
	void resetRackets();
	bool checkForWin();
	void moveRacket1Up();
	void moveRacket1Down();
	void moveRacket2Up();
	void moveRacket2Down();
}
#endif