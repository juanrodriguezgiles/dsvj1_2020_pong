#ifndef STATE_H
#define STATE_H
namespace state {
	enum class State {
		mainMenu,
		options,
		how2Play,
		playing,
		pause,
		newRound,
		playerWins,
		credits,
		controls,
		p1UP,
		p1DOWN,
		playingVsAi,
		modeSelect
	};
	extern State currentState;
}
#endif