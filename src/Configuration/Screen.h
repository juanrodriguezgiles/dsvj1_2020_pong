#ifndef SCREEN_H
#define SCREEN_H
const int screenWidth = 800;
const int screenHeight = 750;
const int reservedSpace = 100;

const int middleTextOffset = 200;
const int middleTextOffSetHow2Play = 360;
const int middleTextOffSetCredits = 350;
const int middleTextOffSetMode = 300;
const int yOffset = 100;
const int spacing = 100;
#endif