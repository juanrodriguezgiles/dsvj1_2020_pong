#include <random>
#include <ctime>
#include "Functions.h"
#include "rackets/rackets.h"
#include "ball/ball.h"
#include "configuration/screen.h"
#include "configuration/state.h"
using namespace Rackets;
using namespace Ball;
using namespace state;
void init() {
	currentState = State::mainMenu;
	srand(time(NULL));
	InitWindow(screenWidth, screenHeight, "PONG");
	SetTargetFPS(60);
}

void initalizePositionsForGame() {
	ballPosition.x = screenWidth / 2;
	ballPosition.y = screenHeight / 2 + reservedSpace;
	ballSpeed.x = 5.0f;
	ballSpeed.y = 5.0f;

	if (!modified) {
		racket1.width = 20;
		racket1.height = 200;
	}

	racket1.x = 50;
	racket1.y = (screenHeight / 2 + reservedSpace) - racket1.height / 2;
	
	racket2.width = 20;
	racket2.height = racket1.height;
	racket2.x = screenWidth - racket2.width - 50;
	racket2.y = (screenHeight / 2 + reservedSpace) - racket2.height / 2;
}

void initalizePositionsForOptions() {
	ballPosition.x = 600;
	ballPosition.y = screenHeight / 2;

	racket1.width = 20;
	racket1.height = 200;
	racket1.x = 500;
	racket1.y = screenHeight / 2 - racket1.height + reservedSpace * 2;
}

void resetRacketScore() {
	racket1Score = 0;
	racket2Score = 0;
}