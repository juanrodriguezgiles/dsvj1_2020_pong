#ifndef AI_H
#define AI_H
#include "rackets/rackets.h"
#include "configuration/screen.h"
#include "ball/ball.h"
namespace AI {
	extern bool playingVsAi;

	void moveRacket();
}

#endif