#ifndef MENU_H
#define MENU_H
#include <random>
#include "raylib.h"
#include "configuration/screen.h"
#include "rackets/rackets.h"
namespace Ball {
	extern Vector2 ballPosition;
	extern Vector2 ballSpeed;
	extern float ballRadius;
	extern Color ballColor;
	extern int ballCurrentColor;
	extern int counter;

	void moveBall();
	void checkForCollisionWithY();
	bool checkForGoal();
	void resetBall();
}
#endif