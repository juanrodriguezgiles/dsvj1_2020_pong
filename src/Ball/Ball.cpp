#include "Ball.h"
using namespace Rackets;
namespace Ball {
	Vector2 ballPosition;
	Vector2 ballSpeed;
	float ballRadius = 20;
	Color ballColor = WHITE;
	int ballCurrentColor = 0;
	int counter = -1;

	void moveBall() {
		ballPosition.x += ballSpeed.x;
		ballPosition.y += ballSpeed.y;
	}
	void checkForCollisionWithY() {
		Rectangle reservedRectangle;
		reservedRectangle.x = 0;
		reservedRectangle.y = 0;
		reservedRectangle.width = float(GetScreenWidth());
		reservedRectangle.height = reservedSpace;

		if ((ballPosition.y >= (GetScreenHeight() - ballRadius)) || (CheckCollisionCircleRec(ballPosition, ballRadius, reservedRectangle))) {
			ballSpeed.y *= -1.0f;
		}
	}
	bool checkForGoal() {
		if (ballPosition.x >= (GetScreenWidth() - ballRadius)) {
			racket1Score++;
			return true;
		}
		else if (ballPosition.x <= ballRadius) {
			racket2Score++;
			return true;
		}
		else {
			return false;
		}
	}
	void resetBall() {
		ballPosition.x = float(GetScreenWidth() / 2);
		ballPosition.y = float(GetScreenHeight() / 2);
		if (rand() % 2 == 0) {
			ballSpeed.x *= -1.0f;
			ballSpeed.y *= -1.0f;
		}
	}
}