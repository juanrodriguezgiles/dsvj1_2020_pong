#ifndef RANDOM_POWER_UP_H
#define RANDOM_POWER_UP_H
#include <random>
#include "configuration/functions.h"
#include "random_power_up/multi_ball.h"
#include "random_power_up/obstacles.h"
#include "random_power_up/invert_controls.h"
#include "random_power_up/change_direction.h"
#include "controls/controls.h"
enum class PowerUP {
	MultiBall,
	Obstacle,
	ChangeDirection,
	InvertControls
};
#endif