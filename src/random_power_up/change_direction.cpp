#include "change_direction.h"
using namespace Ball;
namespace ChangeDirection {
	bool active = false;

	void ChangeDirection() {
		ballSpeed.x *= -1.0f;
		ballSpeed.y *= -1.0f;
		active = false;
	}

	void spawnChangeDirection() {
		active = true;
	}
}