#include "multi_ball.h"
using namespace Rackets;
namespace MultiBall {
	bool active = false;
	double time = 0;
	Vector2 position;
	Vector2 speed;
	float mradius = 20;
	Color mcolor = RED;
	int counter = -1;

	void multiBall() {
		if (time + 10 < GetTime()) {
			active = false;
		}
		else {
			moveBall();
			checkForCollisionWithY();
			checkForCollisionWithRackets();
		}
	}

	void spawnMultiBall() {
		position.x = float(GetScreenWidth() / 2);
		position.y = float(GetScreenHeight() / 2 - reservedSpace);
		speed.x = 5.0f;
		speed.y = 5.0f;
		time = GetTime();
		active = true;
	}

	void moveBall() {
		position.x += speed.x;
		position.y += speed.y;
	}

	void checkForCollisionWithY() {
		Rectangle reservedRectangle;
		reservedRectangle.x = 0;
		reservedRectangle.y = 0;
		reservedRectangle.width = float(GetScreenWidth());
		reservedRectangle.height = reservedSpace;

		if ((position.y >= float((GetScreenHeight() - mradius))) || (CheckCollisionCircleRec(position, mradius, reservedRectangle))) {
			speed.y *= -1.0f;
		}
	}

	bool checkForGoal() {
		if (position.x >= (GetScreenWidth() - mradius)) {
			racket1Score++;
			return true;
		}
		else if (position.x <= mradius) {
			racket2Score++;
			return true;
		}
		else {
			return false;
		}
	}

	void checkForCollisionWithRackets() {
		if (counter == -1) {
			if (CheckCollisionCircleRec(position, mradius, racket1)) {
				speed.x *= -1.0f;
				speed.y *= -1.0f;
				counter++;
			}
			if (CheckCollisionCircleRec(position, mradius, racket2)) {
				speed.x *= -1.0f;
				speed.y *= -1.0f;
				counter++;
			}
		}
		if (counter > -1 && counter <= 6) {
			counter++;
		}
		else if (counter > 6) {
			counter = -1;
		}
	}
}