#include "obstacles.h"
using namespace Ball;
namespace Obstacles {
	bool active = false;
	double time = 0;
	Rectangle obstacle;

	void Obstacle() {
		if (time + 10 < GetTime()) {
			active = false;
		}
		else {
			checkForCollisionWithObstacle();
		}
	}

	void spawnObstacle() {
		obstacle.x = float(GetScreenWidth() / 2);
		obstacle.y = float(rand() % (screenHeight - reservedSpace + 1) + reservedSpace);
		obstacle.width = 20;
		obstacle.height = 80;
		time = GetTime();
		active = true;
	}

	void checkForCollisionWithObstacle() {
		if (counter == -1) {
			if (CheckCollisionCircleRec(ballPosition, ballRadius, obstacle)) {
				ballSpeed.x *= -1.0f;
				ballSpeed.y *= -1.0f;
				counter++;
			}
		}
		if (counter > -1 && counter <= 6) {
			counter++;
		}
		else if (counter > 6) {
			counter = -1;
		}
	}
}