#ifndef CHANGE_DIRECTION_H
#define CHANGE_DIRECTION_H
#include "raylib.h"
#include "ball/ball.h"
namespace ChangeDirection {
	extern bool active;

	void ChangeDirection();
	void spawnChangeDirection();
}
#endif