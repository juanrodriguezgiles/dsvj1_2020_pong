#include "random_power_up.h"
using namespace Controls;
void deSpawnRandomPowerUP() {
	MultiBall::active = false;
	Obstacles::active = false;
	InvertControls::active = false;
	ChangeDirection::active = false;
	player1UP = KEY_A;
	player1DOWN = KEY_D;
	player2UP = KEY_LEFT;
	player2DOWN = KEY_RIGHT;
}