#ifndef INVERT_CONTROLS_H
#define INVERT_CONTROLS_H
#include "raylib.h"
#include "controls/controls.h"
#pragma once
namespace InvertControls {
	extern bool active;
	extern double time;
	extern int aux;

	void invertControls();
	void spawnInvertControls();
}
#endif