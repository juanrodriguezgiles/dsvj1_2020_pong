#ifndef OBSTACLES_H
#define OBSTACLES_H
#include <random>
#include "raylib.h"
#include "configuration/screen.h"
#include "ball/ball.h"
namespace Obstacles {
	extern bool active;
	extern double time;
	extern Rectangle obstacle;

	void Obstacle();
	void spawnObstacle();
	void checkForCollisionWithObstacle();
}
#endif