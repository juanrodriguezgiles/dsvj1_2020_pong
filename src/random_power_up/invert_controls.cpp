#include "invert_controls.h"
using namespace Controls;
namespace InvertControls {
	bool active = false;
	double time = 0;
	int aux = 0;

	void invertControls() {
		if (time + 10 <= GetTime()) {
			active = false;

			player1UP = KEY_A;
			player1DOWN = KEY_D;
			player2UP = KEY_LEFT;
			player2DOWN = KEY_RIGHT;
		}
	}

	void spawnInvertControls() {
		time = GetTime();
		active = true;

		aux = player1UP;
		player1UP = player1DOWN;
		player1DOWN = aux;

		aux = player2UP;
		player2UP = player2DOWN;
		player2DOWN = aux;
	}
}
