#ifndef MULTI_BALL_H
#define MULTI_BALL_H
#include "raylib.h"
#include "configuration/screen.h"
#include "rackets/rackets.h"
#pragma once
namespace MultiBall {
	extern bool active;
	extern double time;
	extern Vector2 position;
	extern Vector2 speed;
	extern float  mradius;
	extern Color mcolor;
	extern int counter;
	void multiBall();
	void spawnMultiBall();
	void moveBall();
	void checkForCollisionWithY();
	bool checkForGoal();
	void checkForCollisionWithRackets();
}
#endif