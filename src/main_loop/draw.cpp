#include "raylib.h"
#include "configuration/state.h"
#include "configuration/screen.h"
#include "ball/ball.h"
#include "rackets/rackets.h"
#include "random_power_up/multi_ball.h"
#include "random_power_up/obstacles.h"
#include "random_power_up/invert_controls.h"
#include "controls/controls.h"
#include "configuration/state.h"
using namespace Rackets;
using namespace Ball;
using namespace state;
void draw() {
	BeginDrawing();
	ClearBackground(BLACK);
	switch (currentState)
	{
	case State::mainMenu:
		DrawText("PONG", GetScreenWidth() / 4, 50, 150, WHITE);
		DrawText("1. PLAY", GetScreenWidth() / 2 - middleTextOffset, 250, 50, WHITE);
		DrawText("2. OPTIONS", GetScreenWidth() / 2 - middleTextOffset, 350, 50, WHITE);
		DrawText("3. HOW TO PLAY", GetScreenWidth() / 2 - middleTextOffset, 450, 50, WHITE);
		DrawText("4. CREDITS", GetScreenWidth() / 2 - middleTextOffset, 550, 50, WHITE);
		break;
	case State::options:
		DrawText("1. CHANGE RACKETS COLOR", 10, spacing, 25, WHITE);
		DrawText("2. CHANGE RACKETS SIZE", 10, spacing * 2, 25, WHITE);
		DrawText("3. CHANGE BALL COLOR", 10, spacing * 3, 25, WHITE);
		DrawText("4. CHANGE BALL SIZE", 10, spacing * 4, 25, WHITE);
		DrawText("5. CHANGE CONTROLS", 10, spacing * 5, 25, WHITE);
		DrawText("6. RETURN TO MAIN MENU", 10, spacing * 6, 25, WHITE);
		DrawRectangleRec(racket1, racketColor);
		DrawCircleV(ballPosition, ballRadius, ballColor);
		break;
	case State::how2Play:
		DrawText("Move the racket with A/D or Left/Right", GetScreenWidth() / 2 - middleTextOffSetHow2Play, GetScreenHeight() / 4, 25, WHITE);
		DrawText("If the ball enters the other player's zone you score a point", GetScreenWidth() / 2 - middleTextOffSetHow2Play, GetScreenHeight() / 4 + spacing, 25, WHITE);
		DrawText("First player to win 5 points wins the round", GetScreenWidth() / 2 - middleTextOffSetHow2Play, GetScreenHeight() / 4 + spacing + spacing, 25, WHITE);
		DrawText("1. Return to main menu", GetScreenWidth() / 2 - middleTextOffSetHow2Play, GetScreenHeight() / 4 + spacing + spacing + spacing, 25, WHITE);
		break;
	case State::pause:
		DrawText(FormatText("%01i", racket1Score), GetScreenWidth() / 2 - 50, 0, 50, WHITE);
		DrawText(FormatText("%01i", racket2Score), GetScreenWidth() / 2 + 20, 0, 50, WHITE);
		DrawLine(0, 50, GetScreenWidth(), 50, WHITE);
		DrawLine(GetScreenWidth() / 2, 0, GetScreenWidth() / 2, 50, WHITE);
		DrawText("PAUSE", GetScreenWidth() / 2 - middleTextOffset, reservedSpace, 100, WHITE);
		DrawText("1. RESUME", GetScreenWidth() / 2 - middleTextOffset, GetScreenHeight() / 2, 25, WHITE);
		DrawText("2. RETURN TO MAIN MENU", GetScreenWidth() / 2 - middleTextOffset, GetScreenHeight() / 2 + spacing, 25, WHITE);
		DrawRectangleRec(racket1, racketColor);
		DrawRectangleRec(racket2, racketColor);
		break;
	case State::newRound:
		DrawText(FormatText("%01i", racket1Score), GetScreenWidth() / 2 - 35, 0, 50, WHITE);
		DrawText(FormatText("%01i", racket2Score), GetScreenWidth() / 2 + 10, 0, 50, WHITE);
		DrawLine(0, 50, GetScreenWidth(), 50, WHITE);
		DrawLine(GetScreenWidth() / 2, 0, GetScreenWidth() / 2, 50, WHITE);
		DrawText("PRESS SPACE TO START", GetScreenWidth() / 3, GetScreenHeight() / 4, 25, WHITE);
		DrawRectangleRec(racket1, racketColor);
		DrawRectangleRec(racket2, racketColor);
		DrawCircleV(ballPosition, ballRadius, ballColor);
		break;
	case State::playerWins:
		if (racket1Score > racket2Score) {
			DrawText("PLAYER 1 WINS", GetScreenWidth() / 2 - middleTextOffset, reservedSpace, 50, WHITE);
		}
		else {
			DrawText("PLAYER 2 WINS", GetScreenWidth() / 2 - middleTextOffset, reservedSpace, 50, WHITE);
		}
		DrawText("1. PLAY AGAIN", GetScreenWidth() / 2 - middleTextOffset, GetScreenHeight() / 2, 25, WHITE);
		DrawText("2. RETURN TO MAIN MENU", GetScreenWidth() / 2 - middleTextOffset, GetScreenHeight() / 2 + spacing, 25, WHITE);
		break;
	case State::credits:
		DrawText("Coded, designed, and tested by Juan Rodriguez Giles", GetScreenWidth() / 2 - middleTextOffSetCredits, GetScreenHeight() / 2, 25, WHITE);
		DrawText("1. Return to main menu", GetScreenWidth() / 2 - middleTextOffSetCredits, GetScreenHeight() / 2 + spacing, 25, WHITE);
		break;
	case State::controls:
		DrawText("1. CHANGE PLAYER 1 CONTROLS", 10, 100, 25, WHITE);
		DrawText("2. CHANGE PLAYER 2 CONTROLS", 10, 200, 25, WHITE);
		DrawText("3. RETURN TO OPTIONS", 10, 300, 25, WHITE);
		break;
	case State::p1UP:
		DrawText("ENTER A NEW KEY", 10, 100, 25, WHITE);
		DrawText("1. RETURN TO MAIN MENU", 10, 200, 25, WHITE);
		DrawText(FormatText("%02i", Controls::player1UP), 10, 300, 25, WHITE);
		break;
	case State::playing:
	case State::playingVsAi:
		ClearBackground(BLACK);
		DrawText("Rounds won: ", 0, 5, 35, WHITE);
		DrawText("Rounds won: ", GetScreenWidth() / 2 + 140, 5, 35, WHITE);
		DrawText(FormatText("%01i", racket1RoundsWon), 220, 10, 35, WHITE);
		DrawText(FormatText("%01i", racket2RoundsWon), GetScreenWidth() / 2 + 360, 10, 35, WHITE);
		DrawText(FormatText("%01i", racket1Score), GetScreenWidth() / 2 - 35, 0, 50, WHITE);
		DrawText(FormatText("%01i", racket2Score), GetScreenWidth() / 2 + 10, 0, 50, WHITE);
		DrawLine(GetScreenWidth() / 2, 0, GetScreenWidth() / 2, GetScreenHeight(), WHITE);
		DrawLine(0, reservedSpace, GetScreenWidth(), reservedSpace, WHITE);
		DrawRectangleRec(racket1, racketColor);
		DrawRectangleRec(racket2, racketColor);
		DrawCircleV(ballPosition, ballRadius, ballColor);
		if (MultiBall::active) {
			DrawText("MULTIBALL ACTIVE", GetScreenWidth() / 2 - 205, 60, 35, RED);
			DrawCircleV(MultiBall::position, MultiBall::mradius, MultiBall::mcolor);
		}
		else if (Obstacles::active) {
			DrawText("OBSTACLE ACTIVE", GetScreenWidth() / 2 - 205, 60, 35, RED);
			DrawRectangleRec(Obstacles::obstacle, RED);
		}
		else if (InvertControls::active) {
			DrawText("INVERTED CONTROLS ACTIVE", GetScreenWidth() / 2 - 300, 60, 35, RED);
		}
		break;
	case State::modeSelect:
		DrawText("1. PLAYER VS PLAYER", GetScreenWidth() / 2 - middleTextOffSetMode, GetScreenHeight() / 2 - yOffset, 50, WHITE);
		DrawText("2. PLAYER VS AI", GetScreenWidth() / 2 - middleTextOffSetMode, GetScreenHeight() / 2 - yOffset + spacing, 50, WHITE);
	default:
		break;
	}
	EndDrawing();
}