#include "configuration/functions.h"
#include "configuration/screen.h"
#include "configuration/state.h"
#include "rackets/rackets.h"
#include "controls/controls.h"
#include "ai/ai.h"
#include "configuration/state.h"
using namespace state;
void input() {
	switch (currentState)
	{
	case State::mainMenu:
		if (IsKeyPressed(KEY_ONE)) {
			currentState = State::modeSelect;
		}
		else if (IsKeyPressed(KEY_TWO)) {
			initalizePositionsForOptions();
			currentState = State::options;
		}
		else if (IsKeyPressed(KEY_THREE)) {
			currentState = State::how2Play;
		}
		else if (IsKeyPressed(KEY_FOUR)) {
			currentState = State::credits;
		}
		break;
	case State::modeSelect:
		if (IsKeyPressed(KEY_ONE)) {
			AI::playingVsAi = false;
			currentState = State::newRound;
		}
		else if (IsKeyPressed(KEY_TWO)) {
			AI::playingVsAi = true;
			currentState = State::newRound;
		}
		initalizePositionsForGame();
		break;
	case State::options:
		if (IsKeyPressed(KEY_ONE)) {
			changeRacketColor();
		}
		else if (IsKeyPressed(KEY_TWO)) {
			changeRacketSize();
		}
		else if (IsKeyPressed(KEY_THREE)) {
			changeBallColor();
		}
		else if (IsKeyPressed(KEY_FOUR)) {
			changeBallSize();
		}
		else if (IsKeyPressed(KEY_FIVE)) {
			currentState = State::controls;
		}
		else if (IsKeyPressed(KEY_SIX)) {
			currentState = State::mainMenu;
		}
		break;
	case State::how2Play:
		if (IsKeyPressed(KEY_ONE)) {
			currentState = State::mainMenu;
		}
		break;
	case State::pause:
		if (IsKeyPressed(KEY_ONE)) {
			currentState = State::playing;
		}
		else if (IsKeyPressed(KEY_TWO)) {
			currentState = State::mainMenu;
		}
		break;
	case State::newRound:
		if (IsKeyPressed(KEY_SPACE)) {
			if (AI::playingVsAi) {
				currentState = State::playingVsAi;
			}
			else {
				currentState = State::playing;
			}
		}
		break;
	case State::playerWins:
		if (IsKeyPressed(KEY_ONE)) {
			initalizePositionsForGame();
			currentState = State::newRound;
			resetRacketScore();
		}
		else if (IsKeyPressed(KEY_TWO)) {
			currentState = State::mainMenu;
			resetRacketScore();
		}
		break;
	case State::playing:
	case State::playingVsAi:
		if (IsKeyDown(Controls::player1UP) && Rackets::racket1.y > reservedSpace) {

			Rackets::moveRacket1Up();
		}
		else if (IsKeyDown(Controls::player1DOWN) && Rackets::racket1.y < GetScreenHeight() - Rackets::racket1.height) {

			Rackets::moveRacket1Down();
		}
		if (!AI::playingVsAi) {
			if (IsKeyDown(Controls::player2UP) && Rackets::racket2.y > reservedSpace) {

				Rackets::moveRacket2Up();
			}
			else if ((IsKeyDown(Controls::player2DOWN)) && Rackets::racket2.y < GetScreenHeight() - Rackets::racket2.height) {

				Rackets::moveRacket2Down();
			}
		}
		if (IsKeyReleased(KEY_P)) {
			currentState = State::pause;
		}
		break;
	case State::credits:
		if (IsKeyPressed(KEY_ONE)) {
			currentState = State::mainMenu;
		}
		break;
	case State::controls:
		if (IsKeyPressed(KEY_ONE)) {
			currentState = State::p1UP;
		}
		else if (IsKeyPressed(KEY_TWO)) {
			currentState = State::p1DOWN;
		}
		else if (IsKeyPressed(KEY_THREE)) {
			currentState = State::options;
		}
		break;
	case State::p1UP:
		Controls::aux = GetKeyPressed();
		if (IsKeyPressed(KEY_ENTER)) {
			currentState = State::controls;
			Controls::player1UP = Controls::aux;
		}
		if (Controls::waiting) {
			Controls::aux = GetKeyPressed();
		}
		break;
	default:
		break;
	}
}