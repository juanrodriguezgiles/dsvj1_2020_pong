#include "configuration/functions.h"
#include "configuration/state.h"
using namespace state;
void game()
{
	init();
	while (!WindowShouldClose())
	{
		switch (currentState)
		{
		case State::playing:
		case State::playingVsAi:
			input();
			update();
			draw();
			break;
		default:
			input();
			draw();
		}
	}
	deinit();
}