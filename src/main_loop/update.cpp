#include <random>
#include "configuration/screen.h"
#include "rackets/rackets.h"
#include "ball/ball.h"
#include "configuration/state.h"
#include "ai/ai.h"
#include "random_power_up/multi_ball.h"
#include "random_power_up/obstacles.h"
#include "configuration/functions.h"
#include "random_power_up/random_power_up.h"
#include "random_power_up/change_direction.h"
#include "random_power_up/invert_controls.h"
#include "configuration/state.h"
using namespace Rackets;
using namespace Ball;
using namespace state;
#pragma region PLAYING
void update() {
	PowerUP currentPowerUp;

	if (AI::playingVsAi) {
		AI::moveRacket();
	}

	if (!MultiBall::active && !Obstacles::active && !ChangeDirection::active && !InvertControls::active) {
		currentPowerUp = PowerUP(rand() % 600);
		switch (currentPowerUp)
		{
		case PowerUP::MultiBall:
			MultiBall::spawnMultiBall();
			break;
		case PowerUP::Obstacle:
			Obstacles::spawnObstacle();
			break;
		case PowerUP::ChangeDirection:
			ChangeDirection::spawnChangeDirection();
			break;
		case PowerUP::InvertControls:
			InvertControls::spawnInvertControls();
			break;
		default:
			break;
		}
	}
	else if (MultiBall::active) {
		MultiBall::multiBall();
	}
	else if (Obstacles::active) {
		Obstacles::Obstacle();
	}
	else if (ChangeDirection::active) {
		ChangeDirection::ChangeDirection();
	}
	else if (InvertControls::active) {
		InvertControls::invertControls();
	}

	if (MultiBall::active) {
		if (MultiBall::checkForGoal()) {
			if (checkForWin()) {
				currentState = State::playerWins;
			}
			else {
				resetBall();
				resetRackets();
				currentState = State::newRound;
			}
			deSpawnRandomPowerUP();
		}
	}

	moveBall();
	checkForCollisionWithRackets();
	checkForCollisionWithY();

	if (checkForGoal())
	{
		if (checkForWin()) {
			currentState = State::playerWins;
		}
		else {
			resetBall();
			resetRackets();
			currentState = State::newRound;
		}
		deSpawnRandomPowerUP();
	}
}
#pragma endregion PLAYING
#pragma region OPTIONS
void changeRacketColor() {
	Color colors[19] = {
		MAROON, ORANGE, DARKGREEN, DARKBLUE, DARKPURPLE, DARKBROWN,
		RED, GOLD, LIME, BLUE, VIOLET, BROWN, PINK, YELLOW,
	   GREEN, SKYBLUE, PURPLE, BEIGE, WHITE
	};

	racketColor = colors[racketCurrentColor];
	racketCurrentColor++;
	if (racketCurrentColor == 18) {
		racketCurrentColor = 0;
	}
}

void changeBallColor() {
	Color colors[19] = {
		MAROON, ORANGE, DARKGREEN, DARKBLUE, DARKPURPLE, DARKBROWN,
		RED, GOLD, LIME, BLUE, VIOLET, BROWN, PINK, YELLOW,
	   GREEN, SKYBLUE, PURPLE, BEIGE, WHITE
	};

	ballColor = colors[ballCurrentColor];
	ballCurrentColor++;
	if (ballCurrentColor == 18) {
		ballCurrentColor = 0;
	}
}

void changeRacketSize() {
	modified = true;
	if (racket1.height > 250) {
		racket1.height = 100;
		racket2.height = 100;
	}
	racket1.height += 10;
	racket2.height += 10;
}

void changeBallSize() {
	if (ballRadius > 50) {
		ballRadius = 5;
	}
	ballRadius += 5;
}
#pragma endregion OPTIONS